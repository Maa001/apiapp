class UsersController < ApiController
	before_action :require_login, except: [:create]

	def create 
      user = User.create(user_params)
      render json: {token: user.auth_token}
	end

	def home
      user = User.find_by_auth_token!(request.headers[:token])
      patients = user.patients
      doctors  = user.doctors
      appointments = user.appointments
      render json: {user: {email: user.email, username: user.username}, patients: patients, doctors: doctors,
                     appointments: appointments}
	end

	private
      
      def user_params
        params.require(:user).permit(:username,:email,:password)
      end
end
