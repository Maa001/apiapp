class PatientsController < ApiController
  before_action :require_login

  def index
    patients = Patient.all
    render json: {patients: patients}
  end

  def show
    patient = Patient.find(params[:id])
    render json: {patient: patient}
  end

  def create
  	patient = Patient.new(patient_params)
  	patient.user = current_user

  	if patient.save!
  		render json: {message: 'ok', patient: patient}
  	else
  		render json: {message: "Could not create patient"}
  	end
  end

  private
    def patient_params
      params.require(:patient).permit(:name,:phone_number)
    end
end
