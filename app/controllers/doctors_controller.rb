class DoctorsController < ApiController
  before_action :require_login

  def index
    doctors = Doctor.all
    render json: {doctors: doctors}
  end

  def show
    doctor = Doctor.find(params[:id])
    render json: {doctor: doctor}
  end

  def create
  	doctor = Doctor.new(doctor_params)
  	doctor.user = current_user
  	if doctor.save!
  		render json: {message: 'ok', doctor: doctor}
  	else
  		render json: {message: "Could not create doctor"}
  	end
  end

  private
    def doctor_params
      params.require(:doctor).permit(:name,:phone_number,:specialization)
    end
end
