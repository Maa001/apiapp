class AppointmentsController < ApiController
   before_action :require_login

  def index
    appointments = Appointment.all
    render json: {appointments: appointments}
  end

  def show
    appointment = Appointment.find(params[:id])
    render json: {appointment: appointment}
  end

  def create
  	# binding.pry
  	appointment = Appointment.new(appointment_params)
  	appointment.user = current_user
  	appointment.patient = Patient.find(params[:patient_id])
  	appointment.doctor =  Doctor.find(params[:doctor_id])
  
  	if appointment.save!
  		render json: {message: 'ok', appointment: appointment}
  	else
  		render json: {message: "Could not create appointments"}
  	end
  end

  private
    def appointment_params
      params.require(:appointment).permit(:disease,:patient_id,:doctor_id)
    end
end
