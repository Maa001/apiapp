class User < ApplicationRecord
  validates_uniqueness_of :username,:email
  has_secure_password
  has_secure_token :auth_token
  
  has_many :patients
  has_many :doctors
  has_many :appointments

  def invalidate_token
  	# binding.pry
    self.update_columns(auth_token: nil)
  end

  def self.validate_login(email,password)
    user = find_by(:email=> email)
    if user && user.authenticate(password)
    	user
    end
  end


end
