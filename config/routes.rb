Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post '/login' => "sessions#create"
  delete '/logout' => "sessions#destroy"
  resources :users
  post '/patients' => "patients#create"
  get '/patients' => "patients#index"
  get '/patients/:id'=> "patients#show"

  get '/doctors' => "doctors#index"
  get '/doctors/:id' => "doctors#show"
  post '/doctors' => "doctors#create"

  get '/appointments' => "appointments#index"
  get '/appointments/:id' => "appointments#show"
  post '/appointments' => "appointments#create"
  get '/home' => "users#home"
end
